
state={
	id=291
	name="STATE_291"
	resources={
		oil=15.000
	}

	history={
		owner = TUR
		victory_points = {
			2097 10
		}
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 1
			air_base = 4

		}
		add_core_of = TUR

	}

	provinces={
		2097 13327 
	}
	manpower=2044165
	buildings_max_level_factor=1.000
	state_category=town
}
