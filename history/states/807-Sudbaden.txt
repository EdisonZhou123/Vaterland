
state={
	id=807
	name="STATE_807"

	history={
		owner = GER
		victory_points = {
			6712 2
		}
		buildings = {
			infrastructure = 6
			arms_factory = 2
		}
		add_core_of = GER
		set_demilitarized_zone = no
		1936.3.7 = {
			set_demilitarized_zone = no

		}
		1939.1.1 = {
			buildings = {
				arms_factory = 3

			}

		}
		1946.1.1 = {
			owner = GER
			controller = GER

		}
		1950.1.1 = {
			owner = FRG

		}

	}

	provinces={
		3692 6542 6712 11640 13261 
	}
	manpower=288741
	buildings_max_level_factor=1.000
	state_category=rural
}
