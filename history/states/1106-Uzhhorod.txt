state={
	id=1106
	name="STATE_1106"
	
	history={
		owner = HUN
		victory_points = {
			13634 2
		}
		buildings = {
			infrastructure = 5

		}
		add_core_of = HUN
		1939.9.1 = {
			owner = HUN
			add_core_of = HUN

		}
		1946.1.1 = {
			owner = SOV
			add_core_of = SOV
			add_core_of = UKR
			remove_core_of = HUN

		}
		2000.1.1 = {
			owner = UKR
			remove_core_of = SOV

		}

	}
	
	provinces={
		13634 
	}
	manpower=115163
	buildings_max_level_factor=1.000
	state_category=enclave
}
