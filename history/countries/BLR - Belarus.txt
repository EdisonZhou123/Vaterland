﻿capital = 206 #Minsk

oob = "BLR_1936"

set_research_slots = 3

# Starting tech
set_technology = {
	Small_Arms_1916 = 1
	Small_Arms_1935 = 1
	tech_recon = 1
	tech_support = 1		
	tech_engineers = 1
	tech_military_police = 1
	tech_mountaineers = 1
	truck_1936 = 1
	paratroopers = 1
	gw_artillery = 1
	
	  # PLACEHOLDER
	#  # PLACEHOLDER
	# # PLACEHOLDER
	Fighter_1933 = 1
	Fighter_1936 = 1
	Tactical_Bomber_1933 = 1
	Strategic_Bomber_1933 = 1	
	Strategic_Bomber_1936 = 1
	Naval_Bomber_1936 = 1

	transport = 1
	Mass_Assault = 1
	fleet_in_being = 1
}

set_politics = {
	ruling_party = authoritarian 
	last_election = "1932.11.8"
	election_frequency = 48
	elections_allowed = no	
}
set_popularities = {
	authoritarian = 100

}
create_country_leader = {
    name = "Eitel Friedrich"
    desc = ""
    picture = "P_A_Eitel_Friedrich.tga"
    expire = "1965.1.1"
    ideology = monarchism
    traits = {
        #
    }
}

create_country_leader = {
    name = "Radasłaŭ Astroŭski"
    desc = ""
    picture = "gfx/leaders/Europe/Portrait_Europe_Generic_land_3.dds"
    expire = "1965.1.1"
    ideology = fascism
    traits = {
        #
    }
}

create_country_leader = {
    name = "Panteleimon Ponomarenko"
    desc = ""
    picture = "gfx/leaders/Europe/Portrait_Europe_Generic_1.dds"
    expire = "1965.1.1"
    ideology = marxism_leninism
    traits = {
        #
    }
}

create_country_leader = {
    name = "Adam Stankievič"
    desc = ""
    picture = "gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
    expire = "1965.1.1"
    ideology = trotskyism
    traits = {
        #
    }
}



